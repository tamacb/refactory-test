import 'package:flutter/material.dart';
import 'package:flutterappnomer5todolist/router.dart';
import 'package:get/get.dart';
import 'calendar_page/calendar_page.dart';
import 'home_page/home_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      initialRoute: '/splash',
      getPages: routes(),
      title: 'ToDoLah',
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      supportedLocales: [
        const Locale('es'),
        const Locale('en'),
        const Locale('id'),
      ],
    );
  }
}
