import 'package:flutter/material.dart';

class CustomAppBar extends PreferredSize {
  final List<Widget> title;
  final double height;
  final List<Widget> leadings;
  final List<Widget> actions;

  CustomAppBar({
    this.title = const [],
    this.leadings = const [],
    this.actions = const [],
    this.height = 120,
  });

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: preferredSize.height,
      color: Colors.transparent,
      alignment: Alignment.center,
      child: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              child: Stack(
                children: <Widget>[
                  Positioned(
                    left: 25,
                    top: 5,
                    bottom: 5,
                    child: Row(
                      children: leadings,
                    ),
                  ),
                  Positioned(
                    left: 100,
                    top: 10,
                    bottom: 5,
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: title,
                      ),
                    ),
                  ),
                  Positioned(
                    right: 75,
                    top: 5,
                    bottom: 5,
                    child: Row(
                      children: actions,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
