import 'package:flutter/material.dart';
import 'package:flutterappnomer5todolist/calendar_page/calendar_page.dart';
import 'package:flutterappnomer5todolist/model/todolist_model.dart';
import 'package:flutterappnomer5todolist/todolist/controller_todo_list.dart';
import 'package:flutterappnomer5todolist/todolist/list_todo.dart';
import 'package:get/get.dart';
import '../custom_appbar.dart';
import '../custom_input.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TodoListModel todoListModel;
  final TodoListController _todoListController = Get.put(TodoListController());
  final titleInputController = TextEditingController();
  final timeInputController = TextEditingController();
  final dateInputController = TextEditingController();
  DateTime _selectedDate;

  Future<Null> _pickDate(BuildContext context) async {
    DateTime _datePick = await showDatePicker(
        context: context,
        initialDate: _selectedDate,
        firstDate: DateTime.now(),
        lastDate: DateTime(2030),
        initialDatePickerMode: DatePickerMode.day,
        selectableDayPredicate: (DateTime val) =>
            val.weekday == 6 || val.weekday == 7 ? false : true);
    if (_datePick != null && _datePick != _selectedDate) {
      setState(() {
        _selectedDate = _datePick;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _resetSelectedDate();
  }

  void _addTodoList() {
    _selectedDate = DateTime.now();
    Get.defaultDialog(
        title: 'Add ToDo List',
        titleStyle: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
        middleText: 'Text di tengah',
        backgroundColor: Colors.white,
        radius: 15,
        content: Column(
          children: [
            TextField(
              controller: titleInputController,
              decoration: InputDecoration(labelText: 'title'),
            ),
            SizedBox(
              height: 10.0,
            ),
            TextField(
              controller: timeInputController,
              decoration: InputDecoration(labelText: 'time until'),
            ),
            SizedBox(
              height: 10.0,
            ),
            TextField(
              controller: dateInputController,
              decoration: InputDecoration(labelText: 'date',hintText: '${DateTime.now().day.toString()}-${DateTime.now().month.toString()}-${DateTime.now().year.toString()}'),
            ),
            SizedBox(
              height: 10.0,
            )
          ],
        ),
        textCancel: "Cancel",
        cancelTextColor: Colors.red,
        confirmTextColor: Colors.black,
        onCancel: () {},
        onConfirm: ()=> _todoListController.addtodoList(TodoListModel(
          title: titleInputController.text,
          timeuntil: timeInputController.text,
          date: dateInputController.text
        )),
        buttonColor: Colors.teal);
  }

  void _resetSelectedDate() {
    _selectedDate = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: [
          Text(
            'Hi, Sam !',
            style: TextStyle(
                color: Colors.white, fontSize: 15, fontWeight: FontWeight.w500),
          ),
         Obx(() => Text(
            '${_todoListController.todos.length} Task today',
            style: TextStyle(
                color: Colors.white, fontSize: 19, fontWeight: FontWeight.w500),
          ))
        ],
        leadings: [
          Container(
            width: 50.0,
            height: 50.0,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(50)),
          )
        ],
      ),
      body: Column(
        children: [
          Flexible(flex: 2, child: CalendarPage()),
          Flexible(
              flex: 5,
              child: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: TodoList(),
              ))
        ],
      ),
      bottomNavigationBar: Container(
        color: Colors.blueGrey,
        height: 80,
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            IconButton(
              icon: Icon(Icons.menu),
              color: Colors.white,
              onPressed: () => Get.toNamed('/coba'),
            ),
            Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20), color: Colors.pink),
              child: IconButton(
                  icon: Icon(Icons.add),
                  color: Colors.white,
                  onPressed: () => _addTodoList()),
            ),
            IconButton(
              icon: Icon(Icons.settings),
              color: Colors.white,
              onPressed: () => Get.toNamed('/coba'),
            ),
          ],
        ),
      ),
      backgroundColor: Colors.grey,
    );
  }
}
