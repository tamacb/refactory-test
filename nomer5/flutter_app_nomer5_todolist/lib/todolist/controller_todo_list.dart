import 'package:flutterappnomer5todolist/model/todolist_model.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

class TodoListController extends GetxController {
  var isLoading = true.obs;
  var todos = List<TodoListModel>().obs;

@override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
  }

  Future fetchCarts() async {
    try {
      isLoading(true);
      List<TodoListModel> todosResult = [];
      todos.value = todosResult;
    } catch (e) {
      print(e);
    } finally {
      isLoading(false);
    }
  }

  addtodoList(TodoListModel todoListModel) async {
    try {
      await Future.delayed(Duration(milliseconds: 50))
          .then((value) => todos.add(todoListModel));
      Get.back();
    } catch (e) {
      print(e);
    } finally {
      Get.toNamed('/');
    }
  }
}
