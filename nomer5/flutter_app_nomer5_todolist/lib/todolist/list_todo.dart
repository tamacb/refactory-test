import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'controller_todo_list.dart';

class TodoList extends StatelessWidget {
  final TodoListController _todoListController = Get.put(TodoListController());
  @override
  Widget build(BuildContext context) {
    return Obx(() => Container(
      child: ListView.builder(
        physics: ScrollPhysics(),
        itemCount: _todoListController.todos.length,
        itemBuilder: (context, index)=> Card(
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Container(
              height: 100,
              width: double.infinity,
              child: Row(
                children: [
                  Flexible(
                    flex: 1,
                    child: Container(
                      width: double.infinity,
                      height: 80,
                      child: Text(_todoListController.todos[index].date, style: TextStyle(color: Colors.black),),
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: Column(
                      children: [
                        Container(
                          width: double.infinity,
                          height: 40,
                          child: Text(_todoListController.todos[index].title, style: TextStyle(color: Colors.black)),
                        ),
                       Container(
                          width: double.infinity,
                          height: 40,
                          child: Text(_todoListController.todos[index].timeuntil, style: TextStyle(color: Colors.black)),
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15), color: Colors.red),
                      width: double.infinity,
                      height: 40,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          IconButton(icon: Icon(Icons.check),),
                          Text('Complete'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ),
    ));
  }
}

