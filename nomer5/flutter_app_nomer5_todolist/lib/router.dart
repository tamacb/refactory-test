import 'package:flutterappnomer5todolist/splash_screen.dart';
import 'package:get/get.dart';
import 'coba.dart';
import 'home_page/home_page.dart';

routes() => [
  GetPage(name: "/", page: () => HomePage()),
  GetPage(name: "/coba", page: () => Coba()),
  GetPage(name: "/splash", page: () => SpalshScreen()),

];