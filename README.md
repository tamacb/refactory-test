## Getting Started
## Screenshot
This project is a starting point for a Flutter application.

![ss1](https://gitlab.com/tamacb/refactory-test/uploads/fe0bdf8df3445793b07a87a906def02c/tdl1.PNG)
![ss2](https://gitlab.com/tamacb/refactory-test/uploads/2b7b0a42d4fb98268ce35a398c490560/tdl2.PNG)
![ss3](https://gitlab.com/tamacb/refactory-test/uploads/d8187e0370215c279572044c91f2d6dd/tdl3.PNG)

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
